# README #

### What is this repository for? 

* This is a Demo application made as development test

### How do I get set up? ###

* Import the project. File -> Import -> Existing Maven Projects -> Next -> Browse -> Select Folder demo (demojavaspring/demo) -> Finish
* For running the Project, open the main application file (demo/src/main/java/com/project/demo/DemoApplication.java), and run it as Java Application.
* When the application runs successfully, it shows the message in the console:
    * Tomcat started on port(s): 8080 (http) with context path ''
    * Started DemoApplication in 7.288 seconds (JVM running for 8.722)
* Now, open the browser and invoke the URL http://localhost:8080.
* Dependencies: It is necesary to have the latest JDK installed, I recommend using IntelliJ as IDLE
* Database configuration: Also find it on application properties file(demo/src/main/resources/application.properties)
    url => jdbc:h2:mem:testdb
    driverClassName => org.h2.Driver
    username => sa
    password => password

### Who do I talk to? ###

* Repo owner or admin: Juan Jose Varela Valencia
