package com.project.demo;

import com.maxmind.geoip2.DatabaseReader;
import com.maxmind.geoip2.exception.AddressNotFoundException;
import com.maxmind.geoip2.exception.GeoIp2Exception;
import com.maxmind.geoip2.model.CityResponse;
import com.project.demo.domain.Coin;
import com.project.demo.domain.NavHistory;
import com.project.demo.services.NavHistoryService;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import javax.servlet.http.HttpServletRequest;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.URL;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

//Main Controller used to control navigation and main functions on the application
@Controller
public class HomeController {

    //Wired navHistoryService to be able to access DB
    @Autowired
    NavHistoryService navHistoryService;

    //Home Request mapping function to call home template and execute main functions of the application
    @RequestMapping("/")
    public String home(Model model, HttpServletRequest request) throws IOException, JSONException, GeoIp2Exception {
        //Get Request to access data from the api indicated:
        URL url = new URL("https://api.coingecko.com/api/v3/coins/list?include_platform=true");
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.setRequestMethod("GET");
        con.setRequestProperty("Content-Type", "application/json");
        con.setConnectTimeout(5000);
        con.setReadTimeout(5000);
        con.setInstanceFollowRedirects(false);
        int responseCode = con.getResponseCode();
        //Controlling request response when success connection
        if (responseCode == HttpURLConnection.HTTP_OK) {
            //Buffering response so we can translate in a manipulable object
            BufferedReader in = new BufferedReader(new InputStreamReader(
                    con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
            JSONArray coinList = new JSONArray(response.toString());
            ArrayList<Coin> listdata = new ArrayList<Coin>();

            //Validating Response is not null
            if (coinList != null) {

                //Iterating JSON array
                for (int i=0;i<coinList.length();i++){

                    //Adding each element of JSON array into ArrayList as a Coin Object
                    listdata.add(new Coin((JSONObject) coinList.get(i)));
                }
                //Passing ArrayList to model as an attribute so we can display it in template
                model.addAttribute("response", listdata);
            }
            //Adding NavHistory to DB
            navHistoryService.addNavHistory(getNavHistory(request));
        }else {
            //Controlling errors
            System.out.println("GET request not worked");
        }
        return "home";
    }

    //Function to get location based on IP Address
    private String getIpLocation(String ip) throws IOException, GeoIp2Exception {
        try {
            //Accessing BD of ip based locations
            String dbLocation = "/Users/JuJo/Repositorios/proyect/demo/src/main/resources/GeoLite2-City_20210713/GeoLite2-City.mmdb";
            File database = new File(dbLocation);
            DatabaseReader dbReader = new DatabaseReader.Builder(database).build();

            //Consulting IP Address on DB
            InetAddress ipAddress = InetAddress.getByName(ip);
            CityResponse response = dbReader.city(ipAddress);

            //returning IP Address response into lat/long pair
            String latitude = response.getLocation().getLatitude().toString();
            String longitude = response.getLocation().getLongitude().toString();
            return latitude + "," + longitude;
        } catch (AddressNotFoundException exception){
            //Controlling errors sending zero pair in case of not found Address
            System.out.println(exception.toString());
            return "0,0";
        }
    }

    //Function to create NavHistory object to save in BD
    private NavHistory getNavHistory(HttpServletRequest request) throws IOException, GeoIp2Exception {
        //Date time format
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
        LocalDateTime now = LocalDateTime.now();

        //Main variables obtained based on request object received
        String ipAddress = request.getHeader("X-FORWARDED-FOR");
        InetAddress ip = InetAddress.getByName(ipAddress);
        String location = getIpLocation(ipAddress);
        String device = "";
        String date = dtf.format(now);

        if(request.getHeader("User-Agent").contains("Mobi")) {
            //you're in mobile land
            device = "Mobile";
        } else {
            //nope, this is probably a desktop
            device = "Desktop";
        }

        //Returning new NavHistory Object with variables obtained
        return new NavHistory(
                location,
                ip.toString(),
                device,
                date
        );
    }

    //List Request mapping function to call lists template and display nav history list
    @RequestMapping("/list")
    private String saveNavHistory(Model model){
        //Getting nav history list
        List<NavHistory> listOfHistory = navHistoryService.getAllHistory();
        //passing nav history to model as an attribute
        model.addAttribute("history", listOfHistory);
        return "lists";
    }

}
