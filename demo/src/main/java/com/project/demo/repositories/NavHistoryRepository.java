package com.project.demo.repositories;

import com.project.demo.domain.NavHistory;
import org.springframework.data.repository.CrudRepository;

public interface NavHistoryRepository extends CrudRepository<NavHistory, Long> {
}