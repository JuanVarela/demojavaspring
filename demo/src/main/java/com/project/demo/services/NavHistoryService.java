package com.project.demo.services;

import com.project.demo.domain.NavHistory;
import com.project.demo.repositories.NavHistoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;

// Nav History Service to write and read History objects in DB
@Service
public class NavHistoryService {

    //Autowired annotation to be able to connect service through application
    @Autowired
    public NavHistoryRepository navHistoryRepository;

    //Getter for all history objects archived
    public List<NavHistory> getAllHistory() {

        List<NavHistory> students = new ArrayList<>();

        navHistoryRepository.findAll()
                .forEach(students::add);

        return students;
    }
    //Public method to save new History objects in DB
    public void addNavHistory(NavHistory navHistory) {
        navHistoryRepository.save(navHistory);
    }
}
