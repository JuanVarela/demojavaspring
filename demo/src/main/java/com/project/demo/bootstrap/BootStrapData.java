package com.project.demo.bootstrap;

import com.project.demo.repositories.NavHistoryRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class BootStrapData implements CommandLineRunner {

    private final NavHistoryRepository navHistoryRepository;

    public BootStrapData(NavHistoryRepository navHistoryRepository) {
        this.navHistoryRepository = navHistoryRepository;
    }

    @Override
    public void run(String... args) throws Exception {}
}
