package com.project.demo.domain;

import lombok.Data;
import org.json.JSONException;
import org.json.JSONObject;

//Coin Object create to keep data readable for templates in applications
public @Data
class Coin {
    private String id;
    private String symbol;
    private String name;
    private JSONObject platforms;

    //Public constructor
    public Coin(JSONObject o) throws JSONException {
        this.id = o.getString("id");
        this.symbol = o.getString("symbol");
        this.name = o.getString("name");
        this.platforms = o.getJSONObject("platforms");
    }

    //Getter and Setter functions
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public JSONObject getPlatforms() {
        return platforms;
    }

    public void setPlatforms(JSONObject platforms) {
        this.platforms = platforms;
    }
}
