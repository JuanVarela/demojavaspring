package com.project.demo.domain;

import javax.persistence.*;

// Nav History Model to keep data readable for application
@Entity
public class NavHistory {

    //Id Variable with id and generated value annotation so it is applicable for primary key in DB
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    //Other variables on the entity NavHistory
    private String latLong;
    private String ip;
    private String device;
    private String dateTime;

    //Public constructors used to create new NavHistory objects
    public NavHistory() {
    }

    public NavHistory(
            String latLong,
            String ip,
            String device,
            String dateTime
    ) {
        this.latLong = latLong;
        this.ip = ip;
        this.device = device;
        this.dateTime = dateTime;
    }

    //Getter and Setters for the entity variables
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLatLong() {
        return latLong;
    }

    public void setLatLong(String latLong) {
        this.latLong = latLong;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getDevice() {
        return device;
    }

    public void setDevice(String device) {
        this.device = device;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    //Overriding toString function so it can translate NavHistory objects to String keeping structure
    @Override
    public String toString() {
        return "navHistory{" +
                "id=" + id +
                ", latLong='" + latLong + '\'' +
                ", ip='" + ip + '\'' +
                ", device='" + device + '\'' +
                ", dateTime='" + dateTime + '\'' +
                '}';
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }
}